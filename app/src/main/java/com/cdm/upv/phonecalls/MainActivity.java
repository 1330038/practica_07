package com.cdm.upv.phonecalls;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTabs();
    }

    public void setTabs(){
        TabHost tabH = (TabHost)findViewById(R.id.tabHost);
        tabH.setup();

        TabHost.TabSpec tab1 = tabH.newTabSpec("tab1");
        TabHost.TabSpec tab2 = tabH.newTabSpec("tab2");

        tab1.setIndicator("LLamadas");
        tab1.setContent(R.id.tab_llamar);

        tab2.setIndicator("Mensajes");
        tab2.setContent(R.id.tab_mensajillo);

        tabH.addTab(tab1);
        tabH.addTab(tab2);
    }


    public void llamada(View view){
        EditText et = (EditText)findViewById(R.id.etxt_telefono);
        String tel = "tel:" + et.getText().toString();
        try{
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse(tel));
            startActivity(callIntent);
        }catch (ActivityNotFoundException activityException){
            Log.e("dialing-example", "Call failed", activityException);
        }catch (SecurityException secEx){
            Log.e("dialing-example", "Call failed", secEx);
        }

    }

    public void enviarMensaje(View vw){
        EditText et2 = (EditText)findViewById(R.id.etxt_telMsg);
        String tel = et2.getText().toString();
        et2 = (EditText)findViewById(R.id.etxt_msg);
        String mensaje =et2.getText().toString();

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(tel,null,mensaje,null,null);
        Toast.makeText(getBaseContext(),"Mensaje Exitoso...!",Toast.LENGTH_LONG);

    }
}
